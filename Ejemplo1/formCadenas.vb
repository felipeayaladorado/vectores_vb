﻿Public Class formCadenas
    Dim strLetra As String = ""
    Private Sub btProceso1_Click(sender As Object, e As EventArgs) Handles btProceso1.Click
        'txtDestino.Text = txtOrigen.Text.Length
        'txtDestino.Text = txtOrigen.Text.Substring(0, 1)
        lstTexto.Items.Clear()
        For intK As Integer = 0 To txtOrigen.Text.Length - 1
            lstTexto.Items.Add(txtOrigen.Text.Substring(intK, 1))
        Next
    End Sub

    Private Sub btAdd_Click(sender As Object, e As EventArgs) Handles btAdd.Click
        lstTexto.Items.Add(txtOrigen.Text)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (txtOrigen.Text.Trim.Length < 10) Then
            MsgBox("La cantidad mínima de letras es 10")
            txtOrigen.Focus()
            Exit Sub
        End If
        strLetra = txtOrigen.Text.Substring(txtOrigen.Text.Length - 1, 1)
        If VerificarVocal(strLetra) Then
            MsgBox("SI es una vocal")
        Else
            MsgBox("No es vocal")
        End If

    End Sub

    Public Function VerificarVocal(ByVal Letra As String) As Boolean
        Letra = Letra.ToUpper
        If (Letra = "A") Or (Letra = "E") Or (Letra = "I") Or (Letra = "O") Or (Letra = "U") Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub btBuscar_Click(sender As Object, e As EventArgs) Handles btBuscar.Click
        Dim buscador As New OpenFileDialog
        Dim strExtension As String = ""
        Dim intPosicion As Integer = 0

        buscador.Title = "Seleccione un archivo"
        buscador.Filter = "Documento de Word|*.docx|Documento PDF|*.pdf|Todos los archivos|*.*"
        buscador.ShowDialog()

        txtRuta.Text = buscador.FileName
        txtArchivo.Text = buscador.SafeFileName

        For intK As Integer = 0 To txtArchivo.Text.Length - 1
            strLetra = txtArchivo.Text.Substring(intK, 1)
            If strLetra = "." Then
                intPosicion = intK
                strExtension = txtArchivo.Text.Substring(intPosicion)
            End If
        Next
        txtExtension.Text = strExtension
    End Sub
End Class