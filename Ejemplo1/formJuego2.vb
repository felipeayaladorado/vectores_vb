﻿Imports System.IO

Public Class formJuego2
    Dim strRuta As String = Application.StartupPath & "\palabras.txt"
    Dim Lista(9) As String
    Dim intPalabra As Integer
    Dim intLargo As Integer
    Private Sub formJuego2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim objReader As New StreamReader(strRuta)
            Dim strLinea As String = ""
            Dim P As Integer = -1
            Do
                P += 1
                strLinea = objReader.ReadLine()
                If strLinea Is Nothing Then Exit Do
                Lista(P) = strLinea
            Loop While (strLinea IsNot Nothing)

        Catch ex As Exception
            MsgBox("Se detecto un problema " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub btJugar_Click(sender As Object, e As EventArgs) Handles btJugar.Click
        Randomize()
        intPalabra = Math.Truncate((Rnd() * 10))
        lbPalabra.Text = Lista(intPalabra)
        intLargo = Lista(intPalabra).Length

        If intLargo > 0 Then lbLetra0.Visible = True
        If intLargo > 1 Then lbLetra1.Visible = True
        If intLargo > 2 Then lbLetra2.Visible = True
        If intLargo > 3 Then lbLetra3.Visible = True
        If intLargo > 4 Then lbLetra4.Visible = True
        If intLargo > 5 Then lbLetra5.Visible = True
        If intLargo > 6 Then lbLetra6.Visible = True
        If intLargo > 7 Then lbLetra7.Visible = True
        If intLargo > 8 Then lbLetra8.Visible = True
        If intLargo > 9 Then lbLetra9.Visible = True
        If intLargo > 10 Then lbLetra10.Visible = True
        If intLargo >= 11 Then lbLetra11.Visible = True

    End Sub

    Private Sub txtLetra_KeyDown(sender As Object, e As KeyEventArgs) Handles txtLetra.KeyDown
        'VALIDACIÓN DE QUE SEA UNA LETRA


        'Verificar si la letra ingresada existe en la palabra

        'si exite, exciribir en pantalla

        'Si no existe, debe marcar equivocación y aparece la figura

    End Sub
End Class