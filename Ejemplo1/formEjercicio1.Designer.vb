﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formEjercicio1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtDato = New System.Windows.Forms.TextBox()
        Me.btInsert = New System.Windows.Forms.Button()
        Me.btMostrar = New System.Windows.Forms.Button()
        Me.btMostar2 = New System.Windows.Forms.Button()
        Me.lbMensaje = New System.Windows.Forms.Label()
        Me.txtResultado = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtDato
        '
        Me.txtDato.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDato.Location = New System.Drawing.Point(42, 30)
        Me.txtDato.Name = "txtDato"
        Me.txtDato.Size = New System.Drawing.Size(319, 35)
        Me.txtDato.TabIndex = 0
        '
        'btInsert
        '
        Me.btInsert.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btInsert.Location = New System.Drawing.Point(618, 30)
        Me.btInsert.Name = "btInsert"
        Me.btInsert.Size = New System.Drawing.Size(206, 35)
        Me.btInsert.TabIndex = 1
        Me.btInsert.Text = "Insertar dato"
        Me.btInsert.UseVisualStyleBackColor = True
        '
        'btMostrar
        '
        Me.btMostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMostrar.Location = New System.Drawing.Point(618, 84)
        Me.btMostrar.Name = "btMostrar"
        Me.btMostrar.Size = New System.Drawing.Size(206, 35)
        Me.btMostrar.TabIndex = 1
        Me.btMostrar.Text = "Mostrar Datos"
        Me.btMostrar.UseVisualStyleBackColor = True
        '
        'btMostar2
        '
        Me.btMostar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMostar2.Location = New System.Drawing.Point(382, 137)
        Me.btMostar2.Name = "btMostar2"
        Me.btMostar2.Size = New System.Drawing.Size(206, 35)
        Me.btMostar2.TabIndex = 1
        Me.btMostar2.Text = "Mostrar Datos"
        Me.btMostar2.UseVisualStyleBackColor = True
        '
        'lbMensaje
        '
        Me.lbMensaje.AutoSize = True
        Me.lbMensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbMensaje.ForeColor = System.Drawing.Color.Red
        Me.lbMensaje.Location = New System.Drawing.Point(39, 68)
        Me.lbMensaje.Name = "lbMensaje"
        Me.lbMensaje.Size = New System.Drawing.Size(0, 31)
        Me.lbMensaje.TabIndex = 2
        '
        'txtResultado
        '
        Me.txtResultado.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResultado.Location = New System.Drawing.Point(25, 237)
        Me.txtResultado.Name = "txtResultado"
        Me.txtResultado.Size = New System.Drawing.Size(598, 35)
        Me.txtResultado.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(367, 40)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'formEjercicio1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 298)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lbMensaje)
        Me.Controls.Add(Me.btMostar2)
        Me.Controls.Add(Me.btMostrar)
        Me.Controls.Add(Me.btInsert)
        Me.Controls.Add(Me.txtResultado)
        Me.Controls.Add(Me.txtDato)
        Me.Name = "formEjercicio1"
        Me.Text = "formEjercicio1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtDato As TextBox
    Friend WithEvents btInsert As Button
    Friend WithEvents btMostrar As Button
    Friend WithEvents btMostar2 As Button
    Friend WithEvents lbMensaje As Label
    Friend WithEvents txtResultado As TextBox
    Friend WithEvents Button1 As Button
End Class
