﻿Public Class formEjercicio1
    Dim intP As Integer
    Dim Lista() As String

    Public Sub Guardar()
        intP += 1
        If intP >= Lista.Length Then
            MsgBox("El vector está lleno")
            Exit Sub
        End If
        Lista(intP) = txtDato.Text
        lbMensaje.Text = "Se guardó el dato: " & txtDato.Text
        txtDato.Focus()
        txtDato.SelectAll()
    End Sub

    Private Sub btInsert_Click(sender As Object, e As EventArgs) Handles btInsert.Click
        Guardar()
    End Sub

    Private Sub formEjercicio1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Lista = New String(4) {}
        intP = -1
    End Sub

    Private Sub btMostrar_Click(sender As Object, e As EventArgs) Handles btMostrar.Click
        For K As Integer = 0 To Lista.Length - 1
            MsgBox(Lista(K))
        Next
    End Sub

    Private Sub btMostar2_Click(sender As Object, e As EventArgs) Handles btMostar2.Click
        txtResultado.Text = ""
        For Each elemento As String In Lista
            txtResultado.Text = txtResultado.Text & elemento & ", "

        Next
    End Sub

    Private Sub txtDato_KeyDown(sender As Object, e As KeyEventArgs) Handles txtDato.KeyDown, txtResultado.KeyDown
        If e.KeyCode = Keys.Enter Then
            Guardar()
        End If
    End Sub
End Class