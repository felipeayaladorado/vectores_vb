﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formJuego2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btJugar = New System.Windows.Forms.Button()
        Me.lbPalabra = New System.Windows.Forms.Label()
        Me.txtLetra = New System.Windows.Forms.TextBox()
        Me.lbLetra0 = New System.Windows.Forms.Label()
        Me.lbLetra1 = New System.Windows.Forms.Label()
        Me.lbLetra2 = New System.Windows.Forms.Label()
        Me.lbLetra3 = New System.Windows.Forms.Label()
        Me.lbLetra4 = New System.Windows.Forms.Label()
        Me.lbLetra5 = New System.Windows.Forms.Label()
        Me.lbLetra6 = New System.Windows.Forms.Label()
        Me.lbLetra10 = New System.Windows.Forms.Label()
        Me.lbLetra8 = New System.Windows.Forms.Label()
        Me.lbLetra11 = New System.Windows.Forms.Label()
        Me.lbLetra9 = New System.Windows.Forms.Label()
        Me.lbLetra7 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.CuadroCadenas1 = New WindowsApp2.CuadroCadenas()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btJugar
        '
        Me.btJugar.BackColor = System.Drawing.Color.Green
        Me.btJugar.FlatAppearance.BorderSize = 0
        Me.btJugar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btJugar.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btJugar.ForeColor = System.Drawing.Color.White
        Me.btJugar.Location = New System.Drawing.Point(221, 496)
        Me.btJugar.Name = "btJugar"
        Me.btJugar.Size = New System.Drawing.Size(423, 58)
        Me.btJugar.TabIndex = 5
        Me.btJugar.Text = "JUGAR"
        Me.btJugar.UseVisualStyleBackColor = False
        '
        'lbPalabra
        '
        Me.lbPalabra.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbPalabra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbPalabra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbPalabra.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbPalabra.ForeColor = System.Drawing.Color.White
        Me.lbPalabra.Location = New System.Drawing.Point(35, 435)
        Me.lbPalabra.Name = "lbPalabra"
        Me.lbPalabra.Size = New System.Drawing.Size(795, 47)
        Me.lbPalabra.TabIndex = 4
        Me.lbPalabra.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtLetra
        '
        Me.txtLetra.Font = New System.Drawing.Font("Microsoft Sans Serif", 70.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLetra.Location = New System.Drawing.Point(388, 319)
        Me.txtLetra.MaxLength = 1
        Me.txtLetra.Name = "txtLetra"
        Me.txtLetra.Size = New System.Drawing.Size(94, 113)
        Me.txtLetra.TabIndex = 6
        Me.txtLetra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbLetra0
        '
        Me.lbLetra0.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra0.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra0.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra0.ForeColor = System.Drawing.Color.White
        Me.lbLetra0.Location = New System.Drawing.Point(8, 241)
        Me.lbLetra0.Name = "lbLetra0"
        Me.lbLetra0.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra0.TabIndex = 7
        Me.lbLetra0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra0.Visible = False
        '
        'lbLetra1
        '
        Me.lbLetra1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra1.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra1.ForeColor = System.Drawing.Color.White
        Me.lbLetra1.Location = New System.Drawing.Point(84, 241)
        Me.lbLetra1.Name = "lbLetra1"
        Me.lbLetra1.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra1.TabIndex = 7
        Me.lbLetra1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra1.Visible = False
        '
        'lbLetra2
        '
        Me.lbLetra2.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra2.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra2.ForeColor = System.Drawing.Color.White
        Me.lbLetra2.Location = New System.Drawing.Point(160, 241)
        Me.lbLetra2.Name = "lbLetra2"
        Me.lbLetra2.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra2.TabIndex = 7
        Me.lbLetra2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra2.Visible = False
        '
        'lbLetra3
        '
        Me.lbLetra3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra3.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra3.ForeColor = System.Drawing.Color.White
        Me.lbLetra3.Location = New System.Drawing.Point(236, 241)
        Me.lbLetra3.Name = "lbLetra3"
        Me.lbLetra3.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra3.TabIndex = 7
        Me.lbLetra3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra3.Visible = False
        '
        'lbLetra4
        '
        Me.lbLetra4.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra4.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra4.ForeColor = System.Drawing.Color.White
        Me.lbLetra4.Location = New System.Drawing.Point(312, 241)
        Me.lbLetra4.Name = "lbLetra4"
        Me.lbLetra4.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra4.TabIndex = 7
        Me.lbLetra4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra4.Visible = False
        '
        'lbLetra5
        '
        Me.lbLetra5.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra5.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra5.ForeColor = System.Drawing.Color.White
        Me.lbLetra5.Location = New System.Drawing.Point(388, 241)
        Me.lbLetra5.Name = "lbLetra5"
        Me.lbLetra5.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra5.TabIndex = 7
        Me.lbLetra5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra5.Visible = False
        '
        'lbLetra6
        '
        Me.lbLetra6.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra6.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra6.ForeColor = System.Drawing.Color.White
        Me.lbLetra6.Location = New System.Drawing.Point(464, 241)
        Me.lbLetra6.Name = "lbLetra6"
        Me.lbLetra6.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra6.TabIndex = 7
        Me.lbLetra6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra6.Visible = False
        '
        'lbLetra10
        '
        Me.lbLetra10.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra10.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra10.ForeColor = System.Drawing.Color.White
        Me.lbLetra10.Location = New System.Drawing.Point(768, 241)
        Me.lbLetra10.Name = "lbLetra10"
        Me.lbLetra10.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra10.TabIndex = 7
        Me.lbLetra10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra10.Visible = False
        '
        'lbLetra8
        '
        Me.lbLetra8.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra8.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra8.ForeColor = System.Drawing.Color.White
        Me.lbLetra8.Location = New System.Drawing.Point(616, 241)
        Me.lbLetra8.Name = "lbLetra8"
        Me.lbLetra8.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra8.TabIndex = 7
        Me.lbLetra8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra8.Visible = False
        '
        'lbLetra11
        '
        Me.lbLetra11.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra11.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra11.ForeColor = System.Drawing.Color.White
        Me.lbLetra11.Location = New System.Drawing.Point(844, 241)
        Me.lbLetra11.Name = "lbLetra11"
        Me.lbLetra11.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra11.TabIndex = 7
        Me.lbLetra11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra11.Visible = False
        '
        'lbLetra9
        '
        Me.lbLetra9.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra9.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra9.ForeColor = System.Drawing.Color.White
        Me.lbLetra9.Location = New System.Drawing.Point(692, 241)
        Me.lbLetra9.Name = "lbLetra9"
        Me.lbLetra9.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra9.TabIndex = 7
        Me.lbLetra9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra9.Visible = False
        '
        'lbLetra7
        '
        Me.lbLetra7.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbLetra7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbLetra7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbLetra7.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbLetra7.ForeColor = System.Drawing.Color.White
        Me.lbLetra7.Location = New System.Drawing.Point(540, 241)
        Me.lbLetra7.Name = "lbLetra7"
        Me.lbLetra7.Size = New System.Drawing.Size(70, 75)
        Me.lbLetra7.TabIndex = 7
        Me.lbLetra7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbLetra7.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(634, 57)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(80, 62)
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(634, 125)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(80, 113)
        Me.PictureBox2.TabIndex = 8
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Location = New System.Drawing.Point(548, 125)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(80, 62)
        Me.PictureBox3.TabIndex = 8
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Location = New System.Drawing.Point(720, 125)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(80, 62)
        Me.PictureBox4.TabIndex = 8
        Me.PictureBox4.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Location = New System.Drawing.Point(827, 57)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(80, 181)
        Me.PictureBox5.TabIndex = 8
        Me.PictureBox5.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Location = New System.Drawing.Point(720, 95)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(101, 24)
        Me.PictureBox6.TabIndex = 8
        Me.PictureBox6.TabStop = False
        '
        'CuadroCadenas1
        '
        Me.CuadroCadenas1.Apellidos = ""
        Me.CuadroCadenas1.BackColor = System.Drawing.Color.White
        Me.CuadroCadenas1.Edad = 0
        Me.CuadroCadenas1.Location = New System.Drawing.Point(5, 12)
        Me.CuadroCadenas1.Name = "CuadroCadenas1"
        Me.CuadroCadenas1.NombreCompleto = ""
        Me.CuadroCadenas1.Nombres = ""
        Me.CuadroCadenas1.Size = New System.Drawing.Size(487, 199)
        Me.CuadroCadenas1.TabIndex = 9
        '
        'formJuego2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(924, 566)
        Me.Controls.Add(Me.CuadroCadenas1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lbLetra7)
        Me.Controls.Add(Me.lbLetra5)
        Me.Controls.Add(Me.lbLetra9)
        Me.Controls.Add(Me.lbLetra2)
        Me.Controls.Add(Me.lbLetra11)
        Me.Controls.Add(Me.lbLetra4)
        Me.Controls.Add(Me.lbLetra8)
        Me.Controls.Add(Me.lbLetra1)
        Me.Controls.Add(Me.lbLetra10)
        Me.Controls.Add(Me.lbLetra3)
        Me.Controls.Add(Me.lbLetra6)
        Me.Controls.Add(Me.lbLetra0)
        Me.Controls.Add(Me.txtLetra)
        Me.Controls.Add(Me.btJugar)
        Me.Controls.Add(Me.lbPalabra)
        Me.Name = "formJuego2"
        Me.Text = "formJuego2"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btJugar As Button
    Friend WithEvents lbPalabra As Label
    Friend WithEvents txtLetra As TextBox
    Friend WithEvents lbLetra0 As Label
    Friend WithEvents lbLetra1 As Label
    Friend WithEvents lbLetra2 As Label
    Friend WithEvents lbLetra3 As Label
    Friend WithEvents lbLetra4 As Label
    Friend WithEvents lbLetra5 As Label
    Friend WithEvents lbLetra6 As Label
    Friend WithEvents lbLetra10 As Label
    Friend WithEvents lbLetra8 As Label
    Friend WithEvents lbLetra11 As Label
    Friend WithEvents lbLetra9 As Label
    Friend WithEvents lbLetra7 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents CuadroCadenas1 As CuadroCadenas
End Class
