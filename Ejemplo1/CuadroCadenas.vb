﻿Public Class CuadroCadenas
    Public Property NombreCompleto As String = ""
    Public Property Nombres As String = ""
    Public Property Apellidos As String = ""
    Public Property Edad As Integer = 0

    Private Sub CuadroCadenas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Limpiar()
    End Sub

    Public Sub Limpiar()
        txtNombres.Text = ""
        txtPaterno.Text = ""
        txtMaterno.Text = ""
        dteNacimiento.Value = Now.Date
        NombreCompleto = ""
        Nombres = ""
        Apellidos = ""
        Edad = 0
    End Sub

    Private Sub btOK_Click(sender As Object, e As EventArgs) Handles btOK.Click
        NombreCompleto = txtNombres.Text.Trim & " " & txtPaterno.Text.Trim & " " & txtMaterno.Text.Trim
        Nombres = txtNombres.Text
        Apellidos = txtPaterno.Text.Trim & " " & txtMaterno.Text.Trim
        Edad = DateDiff(DateInterval.Year, dteNacimiento.Value.Date, Now.Date)
        If Edad < 18 Then
            MsgBox("Debe ser mayor de edad para registrarse", MsgBoxStyle.Exclamation)
            dteNacimiento.Value = Now.Date
            Edad = 0
        End If
    End Sub

    Private Sub btClean_Click(sender As Object, e As EventArgs) Handles btClean.Click
        Limpiar()
    End Sub
End Class
