﻿Public Class formPrimos
    Dim inicio, fin As DateTime
    Dim duracion As TimeSpan
    Dim Lista() As Integer
    Private Sub btVerificar_Click(sender As Object, e As EventArgs) Handles btVerificar.Click
        Dim intNumero As Integer
        Dim intDenominador As Integer
        Dim intContador As Integer
        Dim R As Double

        'Validación para verificar que un cuadro de texto no quede vacío
        If txtNumero.Text.Trim = "" Then
            MsgBox("Por favor ingrese un número mayor a 1", MsgBoxStyle.Exclamation)
            txtNumero.Focus()
            txtNumero.SelectAll()
            Exit Sub
        End If


        'validación para verificar que el dato sea numérico
        If Not IsNumeric(txtNumero.Text.Trim) Then
            MsgBox("Por favor ingrese un número mayor a 1", MsgBoxStyle.Exclamation)
            txtNumero.Focus()
            txtNumero.SelectAll()
            Exit Sub
        End If



        intNumero = CInt(txtNumero.Text)

        'validar si el número es positivo mayor a 1
        If (intNumero <= 1) Then
            MsgBox("Por favor ingrese un número mayor a 1", MsgBoxStyle.Exclamation)
            txtNumero.Focus()
            txtNumero.SelectAll()
            Exit Sub
        End If

        intContador = 0
        For intDenominador = 1 To intNumero
            R = intNumero Mod intDenominador
            If (R = 0) Then
                'residuo cero, entonces es división entera
                intContador += 1
            End If
        Next

        If (intContador = 2) Then
            'si el contador es 2, entonces es número SI primo
            MsgBox("El número si es primo", MsgBoxStyle.Information)
        Else
            'si el contador es distinto a 2, entonces es número NO primo
            MsgBox("El número no es primo", MsgBoxStyle.Critical)
        End If
        txtNumero.Focus()
        txtNumero.SelectAll()
    End Sub

    Private Sub btGenerar_Click(sender As Object, e As EventArgs) Handles btGenerar.Click
        Ejecutar()
    End Sub


    Private Sub txtN_KeyDown(sender As Object, e As KeyEventArgs) Handles txtN.KeyDown
        If e.KeyCode = Keys.Enter Then
            Ejecutar()
        End If
    End Sub

    Public Function EsPrimo(ByVal numero As Integer) As Boolean
        Dim intDenominador As Integer
        Dim intContador As Integer
        Dim R As Double
        intContador = 0
        For intDenominador = 1 To numero
            R = numero Mod intDenominador
            If (R = 0) Then
                'residuo cero, entonces es división entera
                intContador += 1
            End If
        Next

        If (intContador = 2) Then
            'si el contador es 2, entonces es número SI primo
            Return True
        Else
            'si el contador es distinto a 2, entonces es número NO primo
            Return False
        End If
    End Function

    Public Sub Ejecutar()
        inicio = DateTime.Now
        Dim intN, intK, intDato As Integer

        'Validación para verificar que un cuadro de texto no quede vacío
        If txtN.Text.Trim = "" Then
            MsgBox("Por favor ingrese un número mayor a 0", MsgBoxStyle.Exclamation)
            txtN.Focus()
            txtN.SelectAll()
            Exit Sub
        End If

        'validación para verificar que el dato sea numérico
        If Not IsNumeric(txtN.Text.Trim) Then
            MsgBox("Por favor ingrese un número mayor a 0", MsgBoxStyle.Exclamation)
            txtN.Focus()
            txtN.SelectAll()
            Exit Sub
        End If

        intN = CInt(txtN.Text)
        'validar si el número es positivo mayor a 1
        If (intN <= 0) Then
            MsgBox("Por favor ingrese un número mayor a 0", MsgBoxStyle.Exclamation)
            txtN.Focus()
            txtN.SelectAll()
            Exit Sub
        End If

        Lista = New Integer(intN - 1) {}
        intK = -1
        intDato = 2
        Do
            If (EsPrimo(intDato)) Then
                intK += 1
                Lista(intK) = intDato
            End If
            intDato += 1
        Loop While (intK < intN - 1)

        'rutina para vaciar el vector a la caja de texto de resultados
        txtLista.Text = ""
        For Each elemento As String In Lista
            txtLista.Text = txtLista.Text & elemento & ", "
        Next
        Lista = Nothing
        GC.Collect()
        fin = DateTime.Now
        duracion = fin - inicio
        txtTiempo.Text = txtTiempo.Text & txtN.Text & "|" & duracion.TotalSeconds & vbCrLf
        MsgBox("Proceso concluido en " & duracion.TotalSeconds & " segundos", MsgBoxStyle.Information)
        txtN.Focus()
        txtN.SelectAll()
    End Sub
End Class