﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formJuego1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formJuego1))
        Me.lbA = New System.Windows.Forms.Label()
        Me.lbB = New System.Windows.Forms.Label()
        Me.lbC = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lbPuntos = New System.Windows.Forms.Label()
        Me.btJugar = New System.Windows.Forms.Button()
        Me.txtRegistro = New System.Windows.Forms.TextBox()
        Me.CuadroCadenas1 = New WindowsApp2.CuadroCadenas()
        Me.SuspendLayout()
        '
        'lbA
        '
        Me.lbA.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbA.Font = New System.Drawing.Font("Microsoft Sans Serif", 80.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbA.ForeColor = System.Drawing.Color.White
        Me.lbA.Location = New System.Drawing.Point(219, 110)
        Me.lbA.Name = "lbA"
        Me.lbA.Size = New System.Drawing.Size(137, 152)
        Me.lbA.TabIndex = 1
        Me.lbA.Text = "0"
        Me.lbA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbB
        '
        Me.lbB.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbB.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbB.Font = New System.Drawing.Font("Microsoft Sans Serif", 80.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbB.ForeColor = System.Drawing.Color.White
        Me.lbB.Location = New System.Drawing.Point(362, 110)
        Me.lbB.Name = "lbB"
        Me.lbB.Size = New System.Drawing.Size(137, 152)
        Me.lbB.TabIndex = 1
        Me.lbB.Text = "0"
        Me.lbB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbC
        '
        Me.lbC.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lbC.Font = New System.Drawing.Font("Microsoft Sans Serif", 80.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbC.ForeColor = System.Drawing.Color.White
        Me.lbC.Location = New System.Drawing.Point(505, 110)
        Me.lbC.Name = "lbC"
        Me.lbC.Size = New System.Drawing.Size(137, 152)
        Me.lbC.TabIndex = 1
        Me.lbC.Text = "0"
        Me.lbC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 50.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(-1, -2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(1171, 93)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "JUEGO DE TRICAS"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(668, 110)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(156, 45)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Puntaje:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbPuntos
        '
        Me.lbPuntos.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbPuntos.Location = New System.Drawing.Point(668, 155)
        Me.lbPuntos.Name = "lbPuntos"
        Me.lbPuntos.Size = New System.Drawing.Size(156, 45)
        Me.lbPuntos.TabIndex = 2
        Me.lbPuntos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btJugar
        '
        Me.btJugar.BackColor = System.Drawing.Color.Green
        Me.btJugar.FlatAppearance.BorderSize = 0
        Me.btJugar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btJugar.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btJugar.ForeColor = System.Drawing.Color.White
        Me.btJugar.Location = New System.Drawing.Point(219, 285)
        Me.btJugar.Name = "btJugar"
        Me.btJugar.Size = New System.Drawing.Size(423, 72)
        Me.btJugar.TabIndex = 3
        Me.btJugar.Text = "JUGAR"
        Me.btJugar.UseVisualStyleBackColor = False
        '
        'txtRegistro
        '
        Me.txtRegistro.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegistro.Location = New System.Drawing.Point(12, 110)
        Me.txtRegistro.Multiline = True
        Me.txtRegistro.Name = "txtRegistro"
        Me.txtRegistro.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRegistro.Size = New System.Drawing.Size(188, 247)
        Me.txtRegistro.TabIndex = 4
        '
        'CuadroCadenas1
        '
        Me.CuadroCadenas1.Apellidos = ""
        Me.CuadroCadenas1.BackColor = System.Drawing.Color.White
        Me.CuadroCadenas1.Edad = 0
        Me.CuadroCadenas1.Location = New System.Drawing.Point(674, 215)
        Me.CuadroCadenas1.Name = "CuadroCadenas1"
        Me.CuadroCadenas1.NombreCompleto = ""
        Me.CuadroCadenas1.Nombres = ""
        Me.CuadroCadenas1.Size = New System.Drawing.Size(491, 196)
        Me.CuadroCadenas1.TabIndex = 5
        '
        'formJuego1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(1170, 419)
        Me.Controls.Add(Me.CuadroCadenas1)
        Me.Controls.Add(Me.txtRegistro)
        Me.Controls.Add(Me.btJugar)
        Me.Controls.Add(Me.lbPuntos)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lbC)
        Me.Controls.Add(Me.lbB)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lbA)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "formJuego1"
        Me.Text = "Juego de tricas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbA As Label
    Friend WithEvents lbB As Label
    Friend WithEvents lbC As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lbPuntos As Label
    Friend WithEvents btJugar As Button
    Friend WithEvents txtRegistro As TextBox
    Friend WithEvents CuadroCadenas1 As CuadroCadenas
End Class
