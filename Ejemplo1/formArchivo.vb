﻿Imports System.IO

Public Class formArchivo
    Dim buscador As New OpenFileDialog
    Dim Save As New SaveFileDialog

    Private Sub btBuscar_Click(sender As Object, e As EventArgs) Handles btBuscar.Click

        Try
            buscador.Title = "Seleccione un archivo"
            buscador.Filter = "Documento de texto|*.txt|Documento HTML|*.html|Todos los archivos|*.*"

            buscador.ShowDialog()

            txtRuta.Text = buscador.FileName
            'If txtRuta.Text = "" Then Exit Sub

            txtContenido.Text = ""
            Dim objReader As New StreamReader(txtRuta.Text)
            Dim strLinea As String = ""
            Do
                strLinea = objReader.ReadLine()
                txtContenido.Text = txtContenido.Text & strLinea & vbCrLf
            Loop While (strLinea IsNot Nothing)

        Catch ex As Exception
            MsgBox("Se detecto un problema " & ex.Message, MsgBoxStyle.Critical)
        End Try


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Save.InitialDirectory = Application.StartupPath
        Save.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*"
        Save.CheckPathExists = True
        Save.FileName = ""
        Save.ShowDialog()
        If Save.FileName.Trim = "" Then Exit Sub

        'Dim strFila As String = ""
        Dim strTexto As String = ""
        'For intF As Integer = 0 To 9
        '    strFila = intF
        '    strTexto = strTexto & strFila & vbCrLf
        'Next
        strTexto = txtContenido.Text
        Dim strArchivo As String = ""
        strArchivo = Save.FileName.Trim

        Dim n_File As Integer
        n_File = FreeFile()
        FileOpen(n_File, strArchivo, OpenMode.Output, OpenAccess.Write)
        Print(n_File, strTexto)
        FileClose()
        MsgBox("Archivo guardado", MsgBoxStyle.Information)
    End Sub
End Class